const Gpio = require('onoff').Gpio

const led = new Gpio(18, 'out')
const engine = new Gpio(3, 'out')

const fireForSeconds = parseInt(process.argv[2] || 2)

engine.writeSync(1);

function testFire(onOff){
  led.writeSync(onOff === 'on' ? 1 : 0)
  engine.writeSync(onOff === 'on' ? 0 : 1)
  
}


console.log('======== TEST FIRE BEGINNING ========')
console.log('\n\nClear Pad')
console.log('\n\nFiring for ' + fireForSeconds + ' seconds')


setTimeout(function() {
   
  testFire('on')
  
  setTimeout(function() {
    testFire('off')
    console.log('\n\n-- TEST FIRE COMPLETE --')
    console.log('\n\n-- Successful fire! --')
    setTimeout(function() {
      // pass
    }, 3000)
  }, fireForSeconds * 1000)
}, 1500)
